package webBanHang.LAB_02.Controller;

import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ClassPathResource;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.StringUtils;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import webBanHang.LAB_02.Entity.*;
import webBanHang.LAB_02.Service.*;
import webBanHang.LAB_02.Entity.Product;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.UUID;

@Controller
public class AdminController {
    @Autowired
    private ProductService productService;

    @Autowired
    private CategoryService categoryService;
    @Autowired
    private UserServices userServices;
    @Autowired
    private OrderService orderService;
    @Autowired
    private RoleService roleService;

    @GetMapping("/admin/index")
    public String adminIndex() {
        return "admin/index"; // Trả về tên của file HTML (không cần phần mở rộng .html)
    }
    @GetMapping("/admin/product/list")
    public String showProductList(Model model) {
        model.addAttribute("products", productService.getAllProducts());
        return "admin/product/list";
    }
    private String saveImageStatic(MultipartFile image) throws IOException {
        File saveFile = new ClassPathResource("static/images").getFile();
        String fileName = UUID.randomUUID()+ "." + StringUtils.getFilenameExtension(image.getOriginalFilename());
        Path path = Paths.get(saveFile.getAbsolutePath() + File.separator + fileName);
        Files.copy(image.getInputStream(), path);
        return fileName;
    }
    @GetMapping("/admin/product/add")
    public String showAddForm(Model model) {
        model.addAttribute("product", new Product());
        model.addAttribute("categories", categoryService.getAllCategories());  //
        return "/admin/product/add";
    }
    // Process the form for adding a new product
    @PostMapping("/admin/product/add")
    public String addProduct(@Valid Product product, BindingResult result, @RequestParam("image") MultipartFile imageFile, RedirectAttributes redirectAttributes) {
        if (result.hasErrors()) {
            return "/admin/product/add";
        }
        if (!imageFile.isEmpty()) {
            try {
                String imageName = saveImageStatic(imageFile);
                product.setThumails("/images/" + imageName);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        productService.addProduct(product);
        redirectAttributes.addFlashAttribute("message", "Sản phẩm đã được thêm thành công!");
        return "redirect:/admin/product/list";
    }
    @GetMapping("/admin/product/edit/{id}")
    public String showEditForm(@PathVariable Long id, Model model) {
        Product product = productService.getProductById(id)
                .orElseThrow(() -> new IllegalArgumentException("Invalid product Id:" + id));
        model.addAttribute("product", product);
        model.addAttribute("categories", categoryService.getAllCategories());
        return "/admin/product/edit";
    }
    // Process the form for adding a new product
    @PostMapping("/admin/product/edit/{id}")
    public String editProduct(@Valid Product product, BindingResult result, @RequestParam("image") MultipartFile imageFile) throws Exception {
        if (result.hasErrors()) {
            return "/admin/product/edit";
        }
        if (!imageFile.isEmpty()) {
            try {
                String imageName = saveImageStatic(imageFile);
                product.setThumails("/images/" + imageName);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        productService.updateProduct(product, imageFile);
        return "redirect:/admin/product/list";
    }
    @GetMapping("/admin/account/list")
    public String showAllAccount(Model model){
        List<User> users =userServices.getAllUser();
        List<Role> roles = roleService.findAll();
        model.addAttribute("users", users);
        model.addAttribute("roles", roles);
        return "/admin/account/list";
    }
    @GetMapping("/admin/product/delete/{id}")
    public String deleteProduct(@PathVariable Long id) {
        productService.deleteProductById(id);
        return "redirect:/admin/product/list";
    }
    @GetMapping("/admin/category/list")
    public String showAllCategories(Model model){
        List<Category> categories = categoryService.getAllCategories();
        model.addAttribute("categories", categories);
        return "/admin/category/list";
    }
    @GetMapping("/admin/category/delete/{id}")
    public String deleteBook(@PathVariable("id") Long id, Model model) {
        Category category = categoryService.getCategoryById(id)
                .orElseThrow(() -> new IllegalArgumentException("Invalid category Id:" + id));
        categoryService.deleteCategoryById(id);
        model.addAttribute("categories", categoryService.getAllCategories());
        return "redirect:/admin/category/list";
    }
    @GetMapping("/admin/order/list")
    public String showAllOrder(Model model){
        List<Order> orders = orderService.getAllOrder();
        model.addAttribute("orders", orders);
        return "/admin/order/list";
    }
    @PostMapping("/admin/account/addRole")
    public String updateRoleForUser(@RequestParam("userId") Long userId,
                                    @RequestParam("roleId") Long roleId,
                                    RedirectAttributes redirectAttributes) {
        userServices.updateRoleForUser(userId, roleId);
        redirectAttributes.addFlashAttribute("message", "Thay đổi quyền truy cập thành công.");
        return "redirect:/admin/account/list";
    }
    @GetMapping("/admin/category/add")
    public String showAddFormCategory(Model model){
        model.addAttribute("category", new Category());
        return "/admin/category/add";
    }
    @PostMapping("/admin/category/add")
    public String addCategory(@Valid Category category, BindingResult result, @RequestParam("image") MultipartFile imageFile) {
        if (result.hasErrors()) {
            return "/admin/category/add";
        }
        if (!imageFile.isEmpty()) {
            try {
                String imageName = saveImageStatic(imageFile);
                category.setThumnails("/images/" +imageName);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        categoryService.addCategory(category);
        return "redirect:/admin/category/list";
    }
    @GetMapping("/admin/category/edit/{id}")
    public String showUpdateForm(@PathVariable("id") Long id, Model model) {
        Category category = categoryService.getCategoryById(id)
                .orElseThrow(() -> new IllegalArgumentException("Invalid category Id:" + id));
        model.addAttribute("category", category);
        return "/admin/category/edit"; // Corrected path
    }

    // POST request to update category
    @PostMapping("/admin/category/edit/{id}")
    public String updateCategory(@PathVariable("id") Long id, @Valid Category category, BindingResult result, Model model, @RequestParam("image") MultipartFile imageFile) {
        if (result.hasErrors()) {
            return "/admin/category/edit"; // Corrected path
        }
        try{
            categoryService.updateCategory(category, imageFile);
        }catch (Exception ex){
            ex.printStackTrace();
            return "/admin/category/edit";
        }
//            categoryService.updateCategory(category);
//            model.addAttribute("categories", categoryService.getAllCategories());
        return "redirect:/admin/category/list";
    }

//    @PostMapping("/removeRole")
//    public String removeRoleFromUser(@RequestParam int userId, @RequestParam Long roleId) {
//        userServices.removeRoleFromUser(userId, roleId);
//        return "redirect:/admin/users";
//    }
}
