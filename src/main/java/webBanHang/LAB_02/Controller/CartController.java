package webBanHang.LAB_02.Controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import webBanHang.LAB_02.Entity.CartItem;
import webBanHang.LAB_02.Entity.Product;
import webBanHang.LAB_02.Service.CartService;
import webBanHang.LAB_02.Service.ProductService;

import java.util.Optional;

@Controller
@RequestMapping("/cart")
public class CartController {

    @Autowired
    private CartService cartService;
    @Autowired
    private ProductService productService;

    @GetMapping
    public String showCart(Model model) {
        model.addAttribute("cartItems", cartService.getCartItems());
        double totalPrice = 0;
        for(CartItem item : cartService.getCartItems()) {
            totalPrice+= item.getProduct().getPrice()*item.getQuantity();
        }
        model.addAttribute("totalPrice", totalPrice);
        return "/cart/cart";
    }

    @PostMapping("/add")
    public String addToCart(@RequestParam Long productId, @RequestParam int quantity, Model model) {
        Optional<Product> product = productService.getProductById(productId);
        if (quantity <= 0 || quantity > product.get().getQuantity()){
            model.addAttribute("cartItems", cartService.getCartItems());
            model.addAttribute("error", "Quantity out of range");
            double totalPrice = 0;
            for (CartItem item : cartService.getCartItems()){
                totalPrice += item.getProduct().getPrice() * item.getQuantity();
            }
            model.addAttribute("totalPrice", totalPrice);
            return "/cart/cart";
        }
        cartService.addToCart(productId, quantity);
        return "redirect:/cart";
    }

    @GetMapping("/remove/{productId}")
    public String removeFromCart(@PathVariable Long productId) {
        cartService.removeFromCart(productId);
        return "redirect:/cart";
    }

    @GetMapping("/clear")
    public String clearCart() {
        cartService.clearCart();
        return "redirect:/cart";
    }

    @PostMapping("/update")
    public String updateCartItemQuantity(@RequestParam Long productId,
                                         @RequestParam int quantity,
                                         Model model) {
        Optional<Product> product = productService.getProductById(productId);
        if (quantity <=0 || quantity > product.get().getQuantity()){
            model.addAttribute("cartItems", cartService.getCartItems());
            model.addAttribute("error", "Quantity out of range");
            double totalPrice = 0;
            for (CartItem item : cartService.getCartItems()){
                totalPrice += item.getProduct().getPrice() * item.getQuantity();
            }
            model.addAttribute("totalPrice", totalPrice);
            return "/cart/cart";
        }
        cartService.updateCartItemQuantity(productId, quantity);
        return "redirect:/cart"; // Return to cart page regardless of success or error
    }
}
