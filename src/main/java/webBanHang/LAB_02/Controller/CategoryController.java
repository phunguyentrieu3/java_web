    package webBanHang.LAB_02.Controller;


    import jakarta.validation.Valid;
    import lombok.RequiredArgsConstructor;
    import org.springframework.beans.factory.annotation.Autowired;
    import org.springframework.core.io.ClassPathResource;
    import org.springframework.stereotype.Controller;
    import org.springframework.ui.Model;
    import org.springframework.util.StringUtils;
    import org.springframework.validation.BindingResult;
    import org.springframework.web.bind.annotation.*;
    import org.springframework.web.multipart.MultipartFile;
    import webBanHang.LAB_02.Entity.Category;
    import webBanHang.LAB_02.Service.CategoryService;

    import java.io.File;
    import java.io.IOException;
    import java.nio.file.Files;
    import java.nio.file.Path;
    import java.nio.file.Paths;
    import java.util.List;
    import java.util.Optional;
    import java.util.UUID;

    @Controller
    @RequestMapping("/categories")
    @RequiredArgsConstructor
    public class CategoryController {

        @Autowired
        private final CategoryService categoryService;

        @GetMapping("/add")
        public String showAddForm(Model model){
            model.addAttribute("category", new Category());
            return "categories/add";
        }

//        @PostMapping("/add")
//        public String addCategory(@Valid Category category, BindingResult result) {
//            if (result.hasErrors()) {
//                return "categories/add";
//            }
//            categoryService.addCategory(category);
//            return "redirect:/categories/list";
//        }
        @PostMapping("/add")
        public String addCategory(@Valid Category category, BindingResult result, @RequestParam("image") MultipartFile imageFile) {
            if (result.hasErrors()) {
                return "/categories/add";
            }
            if (!imageFile.isEmpty()) {
                try {
                    String imageName = saveImageStatic(imageFile);
                    category.setThumnails("/images/" +imageName);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            categoryService.addCategory(category);
            return "redirect:/categories/list";
        }
        private String saveImageStatic(MultipartFile image) throws IOException {
            File saveFile = new ClassPathResource("static/images").getFile();
            String fileName = UUID.randomUUID()+ "." + StringUtils.getFilenameExtension(image.getOriginalFilename());
            Path path = Paths.get(saveFile.getAbsolutePath() + File.separator + fileName);
            Files.copy(image.getInputStream(), path);
            return fileName;
        }
        @GetMapping("/list")
        public String showAllCategories(Model model){
            List<Category> categories = categoryService.getAllCategories();
            model.addAttribute("categories", categories);
            return "categories/list";
        }
        // GET request to show category edit form
        @GetMapping("/edit/{id}")
        public String showUpdateForm(@PathVariable("id") Long id, Model model) {
            Category category = categoryService.getCategoryById(id)
                    .orElseThrow(() -> new IllegalArgumentException("Invalid category Id:" + id));
            model.addAttribute("category", category);
            return "categories/edit"; // Corrected path
        }

        // POST request to update category
        @PostMapping("/edit/{id}")
        public String updateCategory(@PathVariable("id") Long id, @Valid Category category, BindingResult result, Model model, @RequestParam("image") MultipartFile imageFile) {
            if (result.hasErrors()) {
                return "categories/edit"; // Corrected path
            }
            try{
                categoryService.updateCategory(category, imageFile);
            }catch (Exception ex){
                ex.printStackTrace();
                return "categories/edit";
            }
//            categoryService.updateCategory(category);
//            model.addAttribute("categories", categoryService.getAllCategories());
            return "redirect:/categories/list";
        }
        // GET request for deleting category
        @GetMapping("/delete/{id}")
        public String deleteBook(@PathVariable("id") Long id, Model model) {
            Category category = categoryService.getCategoryById(id)
                    .orElseThrow(() -> new IllegalArgumentException("Invalid category Id:" + id));
            categoryService.deleteCategoryById(id);
            model.addAttribute("categories", categoryService.getAllCategories());
            return "redirect:/categories/list";
        }

    }
