package webBanHang.LAB_02.Controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import webBanHang.LAB_02.Entity.CartItem;
import webBanHang.LAB_02.Entity.Order;
import webBanHang.LAB_02.Service.CartService;
import webBanHang.LAB_02.Service.OrderService;

import java.math.BigDecimal;
import java.util.List;

@Controller
@RequestMapping("/order")
public class OrderController {

    @Autowired
    private OrderService orderService;

    @Autowired
    private CartService cartService;

    @GetMapping("/checkout")
    public String checkout() {
        return "cart/checkout"; // Trang thanh toán
    }

    @PostMapping("/submit")
    public String submitOrder(
            @RequestParam("customerName") String customerName,
            @RequestParam("phone") String phone,
            @RequestParam("email") String email,
            @RequestParam("address") String address,
            @RequestParam("payment") String payment) {
        List<CartItem> cartItems = cartService.getCartItems();
        if (cartItems.isEmpty()) {
            return "redirect:/cart"; // Redirect if cart is empty
        }
        orderService.createOrder(customerName, cartItems, email, phone, address, payment);
        cartService.clearCart(); // Clear cart after successful order
        return "redirect:/order/confirmation";
    }

    @GetMapping("/confirmation")
    public String orderConfirmation(Model model) {
        model.addAttribute("message", "Đơn hàng của bạn đã được đặt thành công.");
        return "cart/order-confirmation";
    }

    @GetMapping("/detail/{orderId}")
    public String showOrderDetail(@PathVariable("orderId") Long orderId, Model model) {
        Order order = orderService.getOrderById(orderId);
        if (order == null) {
            // Xử lý khi không tìm thấy đơn hàng
            return "error"; // Hoặc chuyển hướng đến trang lỗi khác
        }
        model.addAttribute("orderId", orderId);
        model.addAttribute("order", order);
        return "cart/order-detail";
    }

}
