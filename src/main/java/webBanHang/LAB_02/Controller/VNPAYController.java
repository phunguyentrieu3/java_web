package webBanHang.LAB_02.Controller;

import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.websocket.server.PathParam;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import webBanHang.LAB_02.Entity.Order;
import webBanHang.LAB_02.Service.OrderService;
import webBanHang.LAB_02.VNPAY.VNPAYConfig;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.text.SimpleDateFormat;
import java.util.*;

@RestController
@CrossOrigin(origins = "http://localhost:8080")
@RequestMapping("api/v1")
@RequiredArgsConstructor
public class VNPAYController {

    @Autowired
    private OrderService orderService;
    @GetMapping("/vnpay/callback")
    public void paymentCallback(@RequestParam Map<String, String> queryParams, HttpServletResponse response) throws IOException {
        String vnp_ResponseCode = queryParams.get("vnp_ResponseCode");
        Long orderId = Long.valueOf(queryParams.get("orderId"));

        // Verify the secure hash
        String secureHash = queryParams.remove("vnp_SecureHash");
        String hashData = VNPAYConfig.hashAllFields(queryParams);
        if (!secureHash.equals(VNPAYConfig.hmacSHA512(VNPAYConfig.hashSecret, hashData))) {
            // Handle invalid secure hash
            response.sendError(HttpServletResponse.SC_BAD_REQUEST, "Invalid secure hash");
            return;
        }

        // Update the order status based on the response code
        if ("00".equals(vnp_ResponseCode)) {
            orderService.updateOrderStatus(orderId, "PAID");
            response.getWriter().write("Payment successful");
        } else {
            orderService.updateOrderStatus(orderId, "FAILED");
            response.getWriter().write("Payment failed");
        }
    }
   @GetMapping("pay")
    public String getPay(@PathParam("price") long price, @PathParam("id") Integer orderId) throws UnsupportedEncodingException{
       String vnp_Version = "2.1.0";
       String vnp_Command = "pay";
       String orderType = "other";
       long amount = price * 100;
       String bankCode = "NCB";

       String vnp_TxnRef = VNPAYConfig.getRandomNumber(8);
       String vnp_IpAddr = "127.0.0.1";

       String vnp_TmnCode = VNPAYConfig.vnp_tmnCode;

       Map<String, String> vnp_Params = new HashMap<>();
       vnp_Params.put("vnp_Version", vnp_Version);
       vnp_Params.put("vnp_Command", vnp_Command);
       vnp_Params.put("vnp_TmnCode", vnp_TmnCode);
       vnp_Params.put("vnp_Amount", String.valueOf(amount));
       vnp_Params.put("vnp_CurrCode", "VND");

       vnp_Params.put("vnp_BankCode", bankCode);
       vnp_Params.put("vnp_TxnRef", vnp_TxnRef);
       vnp_Params.put("vnp_OrderInfo", "Thanh toan don hang" + vnp_TxnRef);
       vnp_Params.put("vnp_OrderType", orderType);

       vnp_Params.put("vnp_Locale", "vn");
       vnp_Params.put("vnp_ReturnUrl", VNPAYConfig.vnp_returnUrl + "?orderId="+orderId);
       vnp_Params.put("vnp_IpAddr", vnp_IpAddr);

       Calendar cld = Calendar.getInstance(TimeZone.getTimeZone("Etc/GMT+7"));
       SimpleDateFormat formatter = new SimpleDateFormat("yyyyMMddHHmmss");
       String vnp_CreateDate = formatter.format(cld.getTime());
       vnp_Params.put("vnp_CreateDate", vnp_CreateDate);

       cld.add(Calendar.MINUTE, 15);
       String vnp_ExpireDate = formatter.format(cld.getTime());
       vnp_Params.put("vnp_ExpireDate", vnp_ExpireDate);

       List fieldNames = new ArrayList(vnp_Params.keySet());
       Collections.sort(fieldNames);
       StringBuilder hashData = new StringBuilder();
       StringBuilder query = new StringBuilder();
       Iterator itr = fieldNames.iterator();
       while (itr.hasNext()) {
           String fieldName = (String) itr.next();
           String fieldValue = (String) vnp_Params.get(fieldName);
           if ((fieldValue != null) && (fieldValue.length() > 0)) {
               hashData.append(fieldName);
               hashData.append('=');
               hashData.append(URLEncoder.encode(fieldValue, StandardCharsets.US_ASCII.toString()));
               query.append(URLEncoder.encode(fieldName, StandardCharsets.US_ASCII.toString()));
               query.append('=');
               query.append(URLEncoder.encode(fieldValue, StandardCharsets.US_ASCII.toString()));
               if (itr.hasNext()) {
                   query.append('&');
                   hashData.append('&');
               }
           }
       }
       String queryUrl = query.toString();
       String vnp_SecureHash = VNPAYConfig.hmacSHA512(VNPAYConfig.hashSecret, hashData.toString());
       queryUrl += "&vnp_SecureHash=" + vnp_SecureHash;
       String paymentUrl = VNPAYConfig.vnp_PayUrl + "?" + queryUrl;

       return paymentUrl;
   }

}
