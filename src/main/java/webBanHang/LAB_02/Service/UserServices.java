package webBanHang.LAB_02.Service;

import jakarta.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import webBanHang.LAB_02.Entity.Role;
import webBanHang.LAB_02.Entity.User;
import webBanHang.LAB_02.Repository.IRoleRepository;
import webBanHang.LAB_02.Repository.IUserRepository;

import java.util.List;
import java.util.Optional;


@Service
public class UserServices {

    @Autowired
    private IUserRepository userRepository;

    @Autowired
    private IRoleRepository roleRepository;

    public void save(User user){
        userRepository.save(user);
        Long userId = userRepository.getUserIdByUseranme(user.getUsername());
        Long roleId = roleRepository.getRoleIdByName("USER");
        if (roleId != 0 && userId != 0){
            userRepository.addRoleToUser(userId, roleId);
        }
    }
    public List<User> getAllUser() {
        //return productRepository.findAll().stream() //sắp xêp id giảm dần
        //.sorted(Comparator.comparing(Product::getName)).toList();
        return userRepository.findAll();
    }
//    phan quyen them role de co the thay doi role truc tiep
    @Transactional
    public void updateRoleForUser(Long userId, Long roleId) {
        User user = userRepository.findById(userId).orElseThrow(() -> new IllegalArgumentException("Invalid user ID"));
        Role role = roleRepository.findById(roleId).orElseThrow(() -> new IllegalArgumentException("Invalid role ID"));

        // Clear existing roles and set the new role
        user.getRoles().clear();
        user.getRoles().add(role);

        userRepository.save(user);
    }

    public Optional<User> findyUsername(String username) throws UsernameNotFoundException {
        return Optional.ofNullable(userRepository.findByUsername(username));
    }
}
