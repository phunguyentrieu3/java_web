package webBanHang.LAB_02.Service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.context.annotation.SessionScope;
import webBanHang.LAB_02.Entity.CartItem;
import webBanHang.LAB_02.Entity.Product;
import webBanHang.LAB_02.Repository.ProductRepository;

import java.util.ArrayList;
import java.util.List;

@Service
@SessionScope
public class CartService {

    private List<CartItem> cartItems = new ArrayList<>();

    @Autowired
    private ProductRepository productRepository;

    public void addToCart(Long productId, int quantity) {
        Product product = productRepository.findById(productId)
                .orElseThrow(() -> new IllegalArgumentException("Sản phẩm không tồn tại: " + productId));
        CartItem cartItem = cartItems.stream()
                .filter(x -> x.getProduct().getId().equals(product.getId())).findFirst().orElse(null);
        if (cartItem == null){
            cartItems.add(new CartItem(product, quantity));
        }else {
            if (cartItem.getQuantity() + quantity < product.getQuantity()){
                cartItem.setProduct(product);
                cartItem.setQuantity(cartItem.getQuantity() + quantity);
            }else {
                cartItem.setProduct(product);
                cartItem.setQuantity(product.getQuantity());
            }
        }
        // Check if quantity is valid
        if (quantity < 1) {
            throw new IllegalArgumentException("Số lượng phải ít nhất là 1");
        }
        if (quantity > product.getQuantity()) {
            throw new IllegalArgumentException("Số lượng vượt quá số lượng hiện có");
        }
        // Check if the product is already in cart and update quantity
        for (CartItem item : cartItems) {
            if (item.getProduct().getId().equals(productId)) {
                int newQuantity = item.getQuantity() + quantity;
                if (newQuantity > product.getQuantity()) {
                    throw new IllegalArgumentException("Số lượng vượt quá số lượng hiện có");
                }
                item.setQuantity(newQuantity);
                return;
            }
        }
        // Add new item to cart if not already present
        cartItems.add(new CartItem(product, quantity));
    }
    public List<CartItem> getCartItems() {
        return cartItems;
    }

    public void removeFromCart(Long productId) {
        cartItems.removeIf(item -> item.getProduct().getId().equals(productId));
    }

    public void clearCart() {
        cartItems.clear();
    }

    public void updateCartItemQuantity(Long productId, int quantity) {
        // Check if quantity is valid
        if (quantity < 1) {
            throw new IllegalArgumentException("Số lượng phải ít nhất là 1");
        }
        // Find the cart item by productId
        CartItem cartItem = cartItems.stream()
                .filter(item -> item.getProduct().getId().equals(productId))
                .findFirst()
                .orElseThrow(() -> new IllegalArgumentException("Không tìm thấy sản phẩm trong giỏ hàng: " + productId));

        // Check if new quantity is valid against available product quantity
        Product product = productRepository.findById(productId)
                .orElseThrow(() -> new IllegalArgumentException("Sản phẩm không tồn tại: " + productId));

        if (quantity > product.getQuantity()) {
            throw new IllegalArgumentException("Số lượng vượt quá số lượng hiện có");
        }
        // Update cart item quantity
        cartItem.setQuantity(quantity);
    }
    public void updateCart(Long productId, int quantity){
        Product product = productRepository.findById(productId).orElseThrow(() -> new IllegalArgumentException("Product not found" + productId));
        CartItem cartItem = cartItems.stream().filter(x -> x.getProduct().getId().equals(product.getId())).findFirst().orElse(null);
        if (cartItem == null){
            cartItems.add(new CartItem(product, quantity));
        }else {
            cartItem.setProduct(product);
            cartItem.setQuantity(quantity);
        }
    }
}
