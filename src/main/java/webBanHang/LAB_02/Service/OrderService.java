package webBanHang.LAB_02.Service;

import jakarta.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import webBanHang.LAB_02.Entity.*;
import webBanHang.LAB_02.Repository.OrderDetailRepository;
import webBanHang.LAB_02.Repository.OrderRepository;

import java.util.Date;
import java.util.List;
import java.util.Optional;

@Service
public class OrderService {

    @Autowired
    private OrderRepository orderRepository;

    @Autowired
    private OrderDetailRepository orderDetailRepository;

    @Autowired
    private CartService cartService;

    @Autowired
    private ProductService productService;

    @Transactional
    public Order createOrder(String customerName, List<CartItem> cartItems, String email, String phone, String address, String payment) {
        Order order = new Order();
        order.setCustomerName(customerName);
        order.setEmail(email);
        order.setAddress(address);
        order.setPhone(phone);
        order.setPayment(payment);
        order.setOrderDate(new Date());
        double totalPrice = 0;
        for (CartItem item : cartService.getCartItems()){
            totalPrice += item.getProduct().getPrice() * item.getQuantity();
        }
        order.setTotalPrice(totalPrice);
        order = orderRepository.save(order);
        for (CartItem item : cartItems) {
            OrderDetail detail = new OrderDetail();
            detail.setOrder(order);
            detail.setProduct(item.getProduct());
            detail.setQuantity(item.getQuantity());
            Optional<Product> product = productService.getProductById(item.getProduct().getId());
            product.ifPresent(x -> x.setQuantity(x.getQuantity() - item.getQuantity()));
            orderDetailRepository.save(detail);
        }
        return order;
    }

    public Order getOrderById(Long orderId) {
        return orderRepository.findById(orderId).orElse(null);
    }
    public List<Order> getAllOrder() {
        //return productRepository.findAll().stream() //sắp xêp id giảm dần
        //.sorted(Comparator.comparing(Product::getName)).toList();
        List<Order> orders = orderRepository.findAll();
        return orders;
    }
    public void updateOrderStatus(Long orderId, String status) {
        Order order = orderRepository.findById(orderId).orElseThrow(() -> new RuntimeException("Order not found"));
        order.setStatus(status);
        orderRepository.save(order);
    }
}
