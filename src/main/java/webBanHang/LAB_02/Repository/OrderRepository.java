package webBanHang.LAB_02.Repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import webBanHang.LAB_02.Entity.Order;

@Repository
public interface OrderRepository extends JpaRepository<Order, Long> {
}
