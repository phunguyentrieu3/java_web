package webBanHang.LAB_02.Repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import webBanHang.LAB_02.Entity.OrderDetail;

@Repository
public interface OrderDetailRepository extends JpaRepository<OrderDetail, Long> {
    @Modifying
    @Query("DELETE FROM OrderDetail od WHERE od.product.id = :productId")
    void deleteByProductId(@Param("productId") Long productId);
}
