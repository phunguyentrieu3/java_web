package webBanHang.LAB_02.Repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import webBanHang.LAB_02.Entity.Category;

@Repository
public interface CategoryRepository extends JpaRepository<Category, Long > {
}
