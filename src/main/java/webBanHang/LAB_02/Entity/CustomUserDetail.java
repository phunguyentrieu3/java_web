package webBanHang.LAB_02.Entity;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import webBanHang.LAB_02.Repository.IUserRepository;

import java.lang.reflect.Array;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

public class CustomUserDetail implements UserDetails {

    private final User user;
    private final IUserRepository userRepository;

    public CustomUserDetail(User user, IUserRepository userRepository){
        this.user = user;
        this.userRepository = userRepository;
    }
    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return Arrays.stream(userRepository.getRoleOfUser(user.getId()))
                .map(SimpleGrantedAuthority::new)
                .collect(Collectors.toSet());
    }

    @Override
    public String getPassword() {
        return user.getPassword();
    }

    @Override
    public String getUsername() {
        return user.getUsername();
    }

    @Override
    public boolean isAccountNonExpired() {
        return true; //return true tai khoan se khong quan tam den thoi han, va se khong bao gio het han
    }

    @Override
    public boolean isAccountNonLocked() {
        return true; //luon coi tai khoan bi khoa sau 3 lan dang nhap sai
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true; //luon coi thong tin dang nhap da het han ,sau 90 ngay
    }

    @Override
    public boolean isEnabled() {
        return true; //luon coi tai khoan bi vo hieu hoa, va se bi khoa neu khong hoat dong sau 90days
    }
}
