package webBanHang.LAB_02.Entity;

import jakarta.persistence.*;
import lombok.*;
import webBanHang.LAB_02.Validator.ValidCategoryId;
import webBanHang.LAB_02.Validator.ValidUserId;


@Setter
@Getter
@RequiredArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "products")
public class Product {
    @Id
    @GeneratedValue(strategy =  GenerationType.IDENTITY)
    private Long id;

    private String name;
    private double price;
    private String description;
    private int quantity;

    @ManyToOne
    @JoinColumn(name = "category_id")
    @ValidCategoryId
    private Category category;

    private String thumails;

    @ManyToOne
    @JoinColumn(name = "user_id")
    @ValidUserId
    private User user;
}
