package webBanHang.LAB_02.Validator;

import jakarta.validation.Constraint;
import jakarta.validation.Payload;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target({ElementType.TYPE, ElementType.FIELD})
@Retention(RetentionPolicy.RUNTIME)
@Constraint(validatedBy = ValidUsernameValidator.class)
public @interface ValidUsername {
    String message() default "Ten dang nhap khong hop le"; //thong bao khi vi pham rang buoc
    Class<?>[] groups() default {}; //Nhom cac rang buoc lien quan toi nhat
    Class<? extends Payload> [] payload() default {}; //Cung cap them chi tiet ve loi
}
