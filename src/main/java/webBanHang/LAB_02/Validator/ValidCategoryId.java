package webBanHang.LAB_02.Validator;

import jakarta.validation.Constraint;
import jakarta.validation.Payload;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target({ElementType.TYPE, ElementType.FIELD})
@Retention(RetentionPolicy.RUNTIME)
@Constraint(validatedBy = ValidCategoryIdValidator.class)
public @interface ValidCategoryId {
    String message() default "Danh mục không hợp lệ"; //thong bao khi vi pham rang buoc
    Class<?>[] groups() default {}; //Nhom cac rang buoc lien quan toi nhat
    Class<? extends Payload> [] payload() default {}; //Cung cap them chi tiet ve loi
}
