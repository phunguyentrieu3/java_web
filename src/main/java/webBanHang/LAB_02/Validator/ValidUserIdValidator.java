package webBanHang.LAB_02.Validator;

import jakarta.validation.ConstraintValidator;
import jakarta.validation.ConstraintValidatorContext;
import org.springframework.beans.factory.annotation.Autowired;
import webBanHang.LAB_02.Entity.User;
import webBanHang.LAB_02.Repository.IUserRepository;

public class ValidUserIdValidator implements ConstraintValidator<ValidUserId, User> {
    @Autowired
    private IUserRepository userRepository;

    @Override
    public boolean isValid(User user, ConstraintValidatorContext constraintValidatorContext){
        if (user == null)
            return true; // bo qua truong hop user = null
        //return userRepository.existsById(Math.toIntExact(user.getId())); co the thay the = return tren
        return user.getId() != null;

    }
}
